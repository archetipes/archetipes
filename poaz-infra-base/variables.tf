#####################################################################
# Variables Azure K8s - Infra Base
#####################################################################
variable "codigoCuenta" {
description = "Codigo de cuenta de la suscripción."
type = "string"
}
variable "tipoSuscripcion" {
description = "Tipo de suscripción."
type = "string"
}
variable "codigoAplicacion" {
description = "Codigo de aplicacion del portafolio de aplicaciones."
type = "string"
default = "MBBK"
}
variable "nombreApp" {
description = "Nombre de aplicacion o servicio."
type = "string"
}
variable "vnetAddressPrefixDev" {
description = "Red virtual de desarrollo."
type = "string"
}
variable "vnetAddressPrefixCrt" {
description = "Red virtual de certificación."
type = "string"
}
variable "vnetAddressPrefixPrd" {
description = "Red virtual de producción."
type = "string"
}
variable "codResourceGroup" {
description = "Código del grupo de recursos."
type = "string"
default = "rsgr"
}
variable "codLocation" {
description = "Código de ubicación de los recursos."
type = "string"
default = "eu2"
}
variable "ambiente" {
description = "Ambiente a desplegar."
type = "string"
default = "f"
}
variable "correlativoInicial" {
description = "Correlativo inicial de la solución."
type = "string"
default = "01"
}