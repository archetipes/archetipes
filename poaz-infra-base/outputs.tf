#####################################################################
# Outputs Azure K8s - Infra Base
#####################################################################

output "infraName" {
  value = "${lookup(azurerm_template_deployment.poaz-infra-base.outputs, "getName")}"
}