#####################################################################
# Main - Azure K8s - Infra Base
#####################################################################

terraform {
  required_version = "> 0.8.0"
}

provider "azurerm" {
    version = "~>1.5"
}

resource "azurerm_resource_group" "poaz-infra-base" {

#'codResourceGroup','codLocation','codigoAplicacion','ambiente'[0],'correlativoInicial'
  name     = "${format("%s%s%s%s",var.codResourceGroup,var.codLocation,var.ambiente,var.correlativoInicial)}"
  location = "eastus2"
}

resource "azurerm_template_deployment" "poaz-infra-base" {
  name                = "${format("%s%s%s%s%s",var.codigoCuenta,var.tipoSuscripcion,var.codigoAplicacion,var.nombreApp,"-template")}"
  resource_group_name = "${azurerm_resource_group.poaz-infra-base.name}"

  template_body = <<DEPLOY
{
  "$schema": "https://schema.management.azure.com/schemas/2018-05-01/subscriptionDeploymentTemplate.json#",
  "contentVersion": "1.0.0.0",
  "parameters": {
    "codigoAplicacion": {
      "type": "string",
      "maxLength": 4,
      "minLength": 4,
      "metadata": {
        "description": "Codigo de aplicacion del portafolio de aplicaciones."
      }
    },
    "vnetAddressPrefixDev": {
      "type": "string",
      "metadata": {
        "description": "Espacio de direcciones para el ambiente de Desarrollo."
      }
    },
    "vnetAddressPrefixCrt": {
      "type": "string",
      "metadata": {
        "description": "Espacio de direcciones para el ambiente de Certificación."
      }
    },
    "vnetAddressPrefixPrd": {
      "type": "string",
      "metadata": {
        "description": "Espacio de direcciones para el ambiente de Producción."
      }
    }
  },
  "variables": {
    "location": "eastus2",
    "locationLogAnalytics": "eastus",
    "locationStorageShell": "eastus",
    "codLocation": "eu2",
    "codLocationLogAnalytics": "eu1",
    "codLocationStorageShell": "eu1",
    "codResourceGroup": "rsgr",
    "codVirtualNetwork": "vnet",
    "codKeyVault": "akvt",
    "codLogAnalytics": "lgan",
    "codStorage": "stac",
    "ambiente": [
      "f",
      "d",
      "c",
      "p"
    ],
    "correlativoInicial": "01",
    "skuLogAnalytics": "standalone",
    "dataRetentionLogAnalyticsProd": 365,
    "dataRetentionLogAnalyticsNoProd": 185,
    "skuKeyVaultProd": "Premium",
    "skuKeyVaultNoProd": "Standard",
    "skuKeyVaultFamily": "A",
    "objectIdKeyVaultGroupSds": "b2473646-4b20-4c53-bc0c-1015dcd81e5c",
    "objectIdKeyVaultGroupAnalisisSeguridad": "2ca10680-83d2-414d-bf4f-b6213cd9512d",
    "accessPoliciesKeyVaultProd": [
      {
        "objectId": "[variables('objectIdKeyVaultGroupSds')]",
        "tenantId": "[subscription().tenantId]",
        "permissions": {
          "secrets": [
            "Get",
            "List",
            "Set",
            "Delete",
            "Recover",
            "Backup",
            "Restore"
          ],
          "certificates": [
            "Get",
            "List",
            "Update",
            "Create",
            "Import",
            "Delete",
            "Recover",
            "Backup",
            "Restore",
            "ManageContacts",
            "ManageIssuers",
            "GetIssuers",
            "ListIssuers",
            "SetIssuers",
            "DeleteIssuers"
          ]
        }
      },
      {
        "objectId": "[variables('objectIdKeyVaultGroupAnalisisSeguridad')]",
        "tenantId": "[subscription().tenantId]",
        "permissions": {
          "keys": [
            "Get",
            "List",
            "Update",
            "Create",
            "Import",
            "Delete",
            "Recover",
            "Backup",
            "Restore"
          ],
          "secrets": [
            "Get",
            "List",
            "Set",
            "Delete",
            "Recover",
            "Backup",
            "Restore"
          ],
          "certificates": [
            "Get",
            "List",
            "Update",
            "Create",
            "Import",
            "Delete",
            "Recover",
            "Backup",
            "Restore",
            "ManageContacts",
            "ManageIssuers",
            "GetIssuers",
            "ListIssuers",
            "SetIssuers",
            "DeleteIssuers"
          ]
        }
      }
    ],
    "accessPoliciesKeyVaultNoProd": [
      {
        "objectId": "[variables('objectIdKeyVaultGroupSds')]",
        "tenantId": "[subscription().tenantId]",
        "permissions": {
          "keys": [
            "Get",
            "List",
            "Update",
            "Create",
            "Import",
            "Delete",
            "Recover",
            "Backup",
            "Restore"
          ],
          "secrets": [
            "Get",
            "List",
            "Set",
            "Delete",
            "Recover",
            "Backup",
            "Restore"
          ],
          "certificates": [
            "Get",
            "List",
            "Update",
            "Create",
            "Import",
            "Delete",
            "Recover",
            "Backup",
            "Restore",
            "ManageContacts",
            "ManageIssuers",
            "GetIssuers",
            "ListIssuers",
            "SetIssuers",
            "DeleteIssuers"
          ]
        }
      },
      {
        "objectId": "[variables('objectIdKeyVaultGroupAnalisisSeguridad')]",
        "tenantId": "[subscription().tenantId]",
        "permissions": {
          "keys": [
            "Get",
            "List",
            "Update",
            "Create",
            "Import",
            "Delete",
            "Recover",
            "Backup",
            "Restore"
          ],
          "secrets": [
            "Get",
            "List",
            "Set",
            "Delete",
            "Recover",
            "Backup",
            "Restore"
          ],
          "certificates": [
            "Get",
            "List",
            "Update",
            "Create",
            "Import",
            "Delete",
            "Recover",
            "Backup",
            "Restore",
            "ManageContacts",
            "ManageIssuers",
            "GetIssuers",
            "ListIssuers",
            "SetIssuers",
            "DeleteIssuers"
          ]
        }
      }
    ],
    "enableDdosVirtualNetwork": false,
    "nameLogAnalyticsDev": "[bcpcloud.getName(variables('codLogAnalytics'),variables('codLocationLogAnalytics'),parameters('codigoAplicacion'),variables('ambiente')[1],variables('correlativoInicial'))]",
    "nameLogAnalyticsCrt": "[bcpcloud.getName(variables('codLogAnalytics'),variables('codLocationLogAnalytics'),parameters('codigoAplicacion'),variables('ambiente')[2],variables('correlativoInicial'))]",
    "nameLogAnalyticsPrd": "[bcpcloud.getName(variables('codLogAnalytics'),variables('codLocationLogAnalytics'),parameters('codigoAplicacion'),variables('ambiente')[3],variables('correlativoInicial'))]",
    "nameVirtualNetworkDev": "[bcpcloud.getName(variables('codVirtualNetwork'),variables('codLocation'),parameters('codigoAplicacion'),variables('ambiente')[1],variables('correlativoInicial'))]",
    "nameVirtualNetworkCrt": "[bcpcloud.getName(variables('codVirtualNetwork'),variables('codLocation'),parameters('codigoAplicacion'),variables('ambiente')[2],variables('correlativoInicial'))]",
    "nameVirtualNetworkPrd": "[bcpcloud.getName(variables('codVirtualNetwork'),variables('codLocation'),parameters('codigoAplicacion'),variables('ambiente')[3],variables('correlativoInicial'))]",
    "nameKeyVaultDev": "[bcpcloud.getName(variables('codKeyVault'),variables('codLocation'),parameters('codigoAplicacion'),variables('ambiente')[1],variables('correlativoInicial'))]",
    "nameKeyVaultCrt": "[bcpcloud.getName(variables('codKeyVault'),variables('codLocation'),parameters('codigoAplicacion'),variables('ambiente')[2],variables('correlativoInicial'))]",
    "nameKeyVaultPrd": "[bcpcloud.getName(variables('codKeyVault'),variables('codLocation'),parameters('codigoAplicacion'),variables('ambiente')[3],variables('correlativoInicial'))]",
    "nameResourceGroupInfra": "[bcpcloud.getName(variables('codResourceGroup'),variables('codLocation'),parameters('codigoAplicacion'),variables('ambiente')[0],variables('correlativoInicial'))]",
    "nameResourceGroupDev": "[bcpcloud.getName(variables('codResourceGroup'),variables('codLocation'),parameters('codigoAplicacion'),variables('ambiente')[1],variables('correlativoInicial'))]",
    "nameResourceGroupCrt": "[bcpcloud.getName(variables('codResourceGroup'),variables('codLocation'),parameters('codigoAplicacion'),variables('ambiente')[2],variables('correlativoInicial'))]",
    "nameResourceGroupPrd": "[bcpcloud.getName(variables('codResourceGroup'),variables('codLocation'),parameters('codigoAplicacion'),variables('ambiente')[3],variables('correlativoInicial'))]",
    "nameResourceGroups": [
      "[variables('nameResourceGroupInfra')]",
      "[variables('nameResourceGroupDev')]",
      "[variables('nameResourceGroupCrt')]",
      "[variables('nameResourceGroupPrd')]"
    ],
    "nameStorageDev": "[concat(bcpcloud.getName(variables('codStorage'),variables('codLocationStorageShell'),parameters('codigoAplicacion'),variables('ambiente')[1],variables('correlativoInicial')),'cshell')]",
    "settingNameDiagnostic": "AuditLog",
    "storageAccountKind": "Storage",
    "storageAccountSku": "Standard_LRS",
    "supportsHttpsTrafficOnly": true,
    "ipSecurityRestrictions": [
      "200.4.200.130",
      "200.37.27.130",
      "216.244.162.194",
      "216.244.165.194"
    ]
  },
  "functions": [
    {
      "namespace": "bcpcloud",
      "members": {
        "getName": {
          "parameters": [
            {
              "name": "codRecurso",
              "type": "string"
            },
            {
              "name": "location",
              "type": "string"
            },
            {
              "name": "codigoAplicacion",
              "type": "string"
            },
            {
              "name": "ambiente",
              "type": "string"
            },
            {
              "name": "correlativoRecurso",
              "type": "string"
            }
          ],
          "output": {
            "type": "string",
            "value": "[toLower(concat(parameters('codRecurso'), parameters('location'), parameters('codigoAplicacion'), parameters('ambiente'),parameters('correlativoRecurso')))]"
          }
        }
      }
    }
  ],
  "resources": [
    {
      "apiVersion": "2018-05-01",
      "type": "Microsoft.Resources/resourceGroups",
      "name": "[toUpper(variables('nameResourceGroups')[copyIndex('resourceGroupCopy')])]",
      "location": "[variables('location')]",
      "comments": "Creación de los Grupos de Recursos base.",
      "copy": {
        "name": "resourceGroupCopy",
        "count": "[length(variables('nameResourceGroups'))]"
      },
      "properties": {}
    },
    {
      "apiVersion": "2018-05-01",
      "type": "Microsoft.Resources/deployments",
      "name": "resource_infra_deploy",
      "resourceGroup": "[variables('nameResourceGroupInfra')]",
      "comments": "Creacion de los servicios del grupo de recursos de Infraestructura",
      "dependsOn": [
        "[resourceId('Microsoft.Resources/resourceGroups', variables('nameResourceGroupInfra'))]"
      ],
      "properties": {
        "mode": "Incremental",
        "template": {
          "$schema": "https://schema.management.azure.com/schemas/2015-01-01/deploymentTemplate.json#",
          "contentVersion": "1.0.0.0",
          "parameters": {},
          "variables": {},
          "resources": [
            {
              "apiVersion": "2018-11-01",
              "type": "Microsoft.Network/virtualNetworks",
              "name": "[variables('nameVirtualNetworkDev')]",
              "location": "[variables('location')]",
              "comments": "Red virtual para el ambiente de desarrollo",
              "properties": {
                "addressSpace": {
                  "addressPrefixes": [
                    "[parameters('vnetAddressPrefixDev')]"
                  ]
                },
                "enableDdosProtection": "[variables('enableDdosVirtualNetwork')]"
              }
            },
            {
              "apiVersion": "2018-11-01",
              "type": "Microsoft.Network/virtualNetworks",
              "name": "[variables('nameVirtualNetworkCrt')]",
              "location": "[variables('location')]",
              "comments": "Red virtual para el ambiente de certificacion",
              "properties": {
                "addressSpace": {
                  "addressPrefixes": [
                    "[parameters('vnetAddressPrefixCrt')]"
                  ]
                },
                "enableDdosProtection": "[variables('enableDdosVirtualNetwork')]"
              }
            },
            {
              "apiVersion": "2018-11-01",
              "type": "Microsoft.Network/virtualNetworks",
              "name": "[variables('nameVirtualNetworkPrd')]",
              "location": "[variables('location')]",
              "comments": "Red virtual para el ambiente de produccion",
              "properties": {
                "addressSpace": {
                  "addressPrefixes": [
                    "[parameters('vnetAddressPrefixPrd')]"
                  ]
                },
                "enableDdosProtection": "[variables('enableDdosVirtualNetwork')]"
              }
            },
            {
              "apiVersion": "2017-03-15-preview",
              "type": "Microsoft.OperationalInsights/workspaces",
              "name": "[variables('nameLogAnalyticsDev')]",
              "location": "[variables('locationLogAnalytics')]",
              "comments": "Recolector de logs para el ambiente de desarrollo",
              "properties": {
                "sku": {
                  "name": "[variables('skuLogAnalytics')]"
                },
                "retentionInDays": "[variables('dataRetentionLogAnalyticsNoProd')]"
              }
            },
            {
              "apiVersion": "2017-03-15-preview",
              "type": "Microsoft.OperationalInsights/workspaces",
              "name": "[variables('nameLogAnalyticsCrt')]",
              "location": "[variables('locationLogAnalytics')]",
              "comments": "Recolector de logs para el ambiente de certificacion",
              "properties": {
                "sku": {
                  "name": "[variables('skuLogAnalytics')]"
                },
                "retentionInDays": "[variables('dataRetentionLogAnalyticsNoProd')]"
              }
            },
            {
              "apiVersion": "2017-03-15-preview",
              "type": "Microsoft.OperationalInsights/workspaces",
              "name": "[variables('nameLogAnalyticsPrd')]",
              "location": "[variables('locationLogAnalytics')]",
              "comments": "Recolector de logs para el ambiente de produccion",
              "properties": {
                "sku": {
                  "name": "[variables('skuLogAnalytics')]"
                },
                "retentionInDays": "[variables('dataRetentionLogAnalyticsProd')]"
              }
            }
          ]
        }
      }
    },
    {
      "apiVersion": "2018-05-01",
      "type": "Microsoft.Resources/deployments",
      "name": "akvt_stac_deploy_dev",
      "resourceGroup": "[variables('nameResourceGroupDev')]",
      "comments": "Creacion del Key Vault para el ambiente de desarrollo",
      "dependsOn": [
        "[resourceId('Microsoft.Resources/resourceGroups', variables('nameResourceGroupDev'))]",
        "[resourceId('Microsoft.Resources/resourceGroups', variables('nameResourceGroupCrt'))]",
        "[resourceId('Microsoft.Resources/resourceGroups', variables('nameResourceGroupPrd'))]",
        "resource_infra_deploy"
      ],
      "properties": {
        "mode": "Incremental",
        "template": {
          "$schema": "https://schema.management.azure.com/schemas/2015-01-01/deploymentTemplate.json#",
          "contentVersion": "1.0.0.0",
          "parameters": {},
          "variables": {},
          "resources": [
            {
              "apiVersion": "2018-02-14",
              "type": "Microsoft.KeyVault/vaults",
              "name": "[variables('nameKeyVaultDev')]",
              "location": "[variables('location')]",
              "comments": "Creacion del Key Vault para el ambiente de desarrollo",
              "properties": {
                "accessPolicies": "[variables('accessPoliciesKeyVaultNoProd')]",
                "tenantId": "[subscription().tenantId]",
                "sku": {
                  "name": "[variables('skuKeyVaultNoProd')]",
                  "family": "[variables('skuKeyVaultFamily')]"
                }
              },
              "resources": [
                {
                  "type": "providers/diagnosticSettings",
                  "name": "[concat('Microsoft.Insights/', variables('settingNameDiagnostic'))]",
                  "apiVersion": "2017-05-01-preview",
                  "properties": {
                    "name": "[variables('settingNameDiagnostic')]",
                    "storageAccountId": null,
                    "eventHubAuthorizationRuleId": null,
                    "eventHubName": null,
                    "workspaceId": "[concat('/subscriptions/', subscription().subscriptionId, '/resourcegroups/', variables('nameResourceGroupInfra'), '/providers/Microsoft.OperationalInsights/workspaces/', variables('nameLogAnalyticsDev'))]",
                    "logs": [
                      {
                        "category": "AuditEvent",
                        "enabled": true
                      }
                    ],
                    "metrics": [
                      {
                        "category": "AllMetrics",
                        "enabled": true
                      }
                    ]
                  },
                  "dependsOn": [
                    "[concat('Microsoft.KeyVault/vaults/', variables('nameKeyVaultDev'))]"
                  ]
                }
              ]
            },
            {
              "apiVersion": "2018-07-01",
              "type": "Microsoft.Storage/storageAccounts",
              "name": "[variables('nameStorageDev')]",
              "location": "[variables('locationStorageShell')]",
              "comments": "Cuenta de almancenamiento para el uso de az cli a traves del portal de azure.",
              "properties": {
                "supportsHttpsTrafficOnly": "[variables('supportsHttpsTrafficOnly')]",
                "encryption": {
                  "services": {
                    "file": {
                      "enabled": true
                    },
                    "blob": {
                      "enabled": true
                    }
                  },
                  "keySource": "Microsoft.Storage"
                },
                "networkAcls": {
                  "bypass": "AzureServices",
                  "virtualNetworkRules": [],
                  "copy": [
                    {
                      "name": "ipRules",
                      "count": "[length(variables('ipSecurityRestrictions'))]",
                      "input": {
                        "value": "[variables('ipSecurityRestrictions')[copyIndex('ipRules')]]",
                        "action": "Allow"
                      }
                    }
                  ],
                  "defaultAction": "Deny"
                }
              },
              "kind": "[variables('storageAccountKind')]",
              "sku": {
                "name": "[variables('storageAccountSku')]"
              }
            }
          ]
        }
      }
    },
    {
      "apiVersion": "2018-05-01",
      "type": "Microsoft.Resources/deployments",
      "name": "akvt_deploy_crt",
      "resourceGroup": "[variables('nameResourceGroupCrt')]",
      "comments": "Creacion del Key Vault para el ambiente de certificacion",
      "dependsOn": [
        "[resourceId('Microsoft.Resources/resourceGroups', variables('nameResourceGroupDev'))]",
        "[resourceId('Microsoft.Resources/resourceGroups', variables('nameResourceGroupCrt'))]",
        "[resourceId('Microsoft.Resources/resourceGroups', variables('nameResourceGroupPrd'))]",
        "resource_infra_deploy"
      ],
      "properties": {
        "mode": "Incremental",
        "template": {
          "$schema": "https://schema.management.azure.com/schemas/2015-01-01/deploymentTemplate.json#",
          "contentVersion": "1.0.0.0",
          "parameters": {},
          "variables": {},
          "resources": [
            {
              "apiVersion": "2018-02-14",
              "type": "Microsoft.KeyVault/vaults",
              "name": "[variables('nameKeyVaultCrt')]",
              "location": "[variables('location')]",
              "comments": "Creacion del Key Vault para el ambiente de certificacion",
              "properties": {
                "accessPolicies": "[variables('accessPoliciesKeyVaultNoProd')]",
                "tenantId": "[subscription().tenantId]",
                "sku": {
                  "name": "[variables('skuKeyVaultNoProd')]",
                  "family": "[variables('skuKeyVaultFamily')]"
                }
              },
              "resources": [
                {
                  "type": "providers/diagnosticSettings",
                  "name": "[concat('Microsoft.Insights/', variables('settingNameDiagnostic'))]",
                  "apiVersion": "2017-05-01-preview",
                  "properties": {
                    "name": "[variables('settingNameDiagnostic')]",
                    "storageAccountId": null,
                    "eventHubAuthorizationRuleId": null,
                    "eventHubName": null,
                    "workspaceId": "[concat('/subscriptions/', subscription().subscriptionId, '/resourcegroups/', variables('nameResourceGroupInfra'), '/providers/Microsoft.OperationalInsights/workspaces/', variables('nameLogAnalyticsCrt'))]",
                    "logs": [
                      {
                        "category": "AuditEvent",
                        "enabled": true
                      }
                    ],
                    "metrics": [
                      {
                        "category": "AllMetrics",
                        "enabled": true
                      }
                    ]
                  },
                  "dependsOn": [
                    "[concat('Microsoft.KeyVault/vaults/', variables('nameKeyVaultCrt'))]"
                  ]
                }
              ]
            }
          ]
        }
      }
    },
    {
      "apiVersion": "2018-05-01",
      "type": "Microsoft.Resources/deployments",
      "name": "akvt_deploy_prd",
      "resourceGroup": "[variables('nameResourceGroupPrd')]",
      "comments": "Creacion del Key Vault para el ambiente de produccion",
      "dependsOn": [
        "[resourceId('Microsoft.Resources/resourceGroups', variables('nameResourceGroupDev'))]",
        "[resourceId('Microsoft.Resources/resourceGroups', variables('nameResourceGroupCrt'))]",
        "[resourceId('Microsoft.Resources/resourceGroups', variables('nameResourceGroupPrd'))]",
        "resource_infra_deploy"
      ],
      "properties": {
        "mode": "Incremental",
        "template": {
          "$schema": "https://schema.management.azure.com/schemas/2015-01-01/deploymentTemplate.json#",
          "contentVersion": "1.0.0.0",
          "parameters": {},
          "variables": {},
          "resources": [
            {
              "apiVersion": "2018-02-14",
              "type": "Microsoft.KeyVault/vaults",
              "name": "[variables('nameKeyVaultPrd')]",
              "location": "[variables('location')]",
              "comments": "Creacion del Key Vault para el ambiente de produccion",
              "properties": {
                "accessPolicies": "[variables('accessPoliciesKeyVaultProd')]",
                "tenantId": "[subscription().tenantId]",
                "sku": {
                  "name": "[variables('skuKeyVaultProd')]",
                  "family": "[variables('skuKeyVaultFamily')]"
                }
              },
              "resources": [
                {
                  "type": "providers/diagnosticSettings",
                  "name": "[concat('Microsoft.Insights/', variables('settingNameDiagnostic'))]",
                  "apiVersion": "2017-05-01-preview",
                  "properties": {
                    "name": "[variables('settingNameDiagnostic')]",
                    "storageAccountId": null,
                    "eventHubAuthorizationRuleId": null,
                    "eventHubName": null,
                    "workspaceId": "[concat('/subscriptions/', subscription().subscriptionId, '/resourcegroups/', variables('nameResourceGroupInfra'), '/providers/Microsoft.OperationalInsights/workspaces/', variables('nameLogAnalyticsPrd'))]",
                    "logs": [
                      {
                        "category": "AuditEvent",
                        "enabled": true
                      }
                    ],
                    "metrics": [
                      {
                        "category": "AllMetrics",
                        "enabled": true
                      }
                    ]
                  },
                  "dependsOn": [
                    "[concat('Microsoft.KeyVault/vaults/', variables('nameKeyVaultPrd'))]"
                  ]
                }
              ]
            }
          ]
        }
      }
    }
  ]
}
DEPLOY

  # these key-value pairs are passed into the ARM Template's `parameters` block
  parameters = {
    "codigoAplicacion" = "${var.codigoAplicacion}"
    "vnetAddressPrefixDev" = "${var.vnetAddressPrefixDev}"
    "vnetAddressPrefixCrt"= "${var.vnetAddressPrefixCrt}"
    "vnetAddressPrefixPrd"= "${var.vnetAddressPrefixPrd}"
  }
  
  deployment_mode = "Incremental"
  
}